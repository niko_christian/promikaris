<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $table = "berita";
    public function users()
    {
    	return $this->belongsTo('App\User');
    }
    protected $fillable = ['judul','konten','path_gambar', 'link_berita'];

    protected $casts = [
        'id' => 'integer'
    ];
}
