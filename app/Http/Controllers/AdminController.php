<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use DateTime;
use DateTimeZone;
use App\Lapor;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $stopkontakgosong = DB::table('lapor')->where("stopkontakgosong", 'Ya')->count();
        $kabeltidakrapi = DB::table('lapor')->where("kabeltidakrapi", 'Ya')->count();
        $ukurankabelsalah = DB::table('lapor')->where("ukurankabelsalah", 'Ya')->count();
        $kabeltidakterjaga = DB::table('lapor')->where("kabeltidakterjaga", 'Ya')->count();
        $kabeltua = DB::table('lapor')->where("kabeltua", 'Ya')->count();
        $stopkontakbertumpuk = DB::table('lapor')->where("stopkontakbertumpuk", 'Ya')->count();
        //echo $kabeltidakrapi;
        $chart = [$stopkontakgosong,$kabeltidakrapi,$ukurankabelsalah,$kabeltidakterjaga,$kabeltua,$stopkontakgosong];
          
       
        return view('admin', ['user' => $user->name],['chart' => $chart]);
    }
}
