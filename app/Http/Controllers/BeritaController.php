<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use DateTime;
use DateTimeZone;
use App\Berita;
use auth;


class BeritaController extends Controller
{
    public function index(){                
        $data = Berita::get();
        return view('adminberita_dashboard', ['data' => $data]);
    }

    public function beritatambah()
    {
        return view('berita_admintambah');
    }
    public function beritastore(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required',
            'konten' => 'required',
            'link_berita' => 'required',
            'gambarberita' => 'file|image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $namafilegambar='';
        if ($request->file('gambarberita') != "") {
       
        $buktigambar = $request->file('gambarberita');

        $namafilegambar = str_replace(" ", "-", $request->judul).'-' .time(). '.' . $buktigambar->getClientOriginalExtension();

        $buktigambar->move(public_path() . '/berita/', $namafilegambar);
        }
        echo $request->link_berita;
        Berita::create([
            'judul' => $request->judul,
            'konten' => $request->konten,
            'link_berita' => $request->link_berita,
            'path_gambar' => $namafilegambar
        ]);
        return redirect('/admin/berita');
    }
    public function beritaedit($idberita)
    {
        //echo $idberita;
        $data = DB::table('berita')->where('idberita',$idberita)->get();
        return view('berita_adminedit', ['data' => $data ]);
    }
    public function beritaupdate($idberita, Request $request)
    {
        $this->validate($request, [
            'judul' => 'required',
            'konten' => 'required',
    
        ]);
        //echo $idberita;
        
        
        if ($request->file('gambarberita') != "") {
            $buktigambar = $request->file('gambarberita');

           $namafilegambar = $request->judul.'-' .time(). '.' . $buktigambar->getClientOriginalExtension();

            $buktigambar->move(public_path() . '/berita/', $namafilegambar);
            
            DB::table('berita')->where('idberita',$idberita)->update([
                'judul' => $request->judul,
                'konten' => $request->konten,
                'link_berita' => $request->link_berita,
                'path_gambar' =>$namafilegambar,
                'updated_at' => now()
                ]);
            return redirect('/admin/berita');
     
        }
        
        DB::table('berita')->where('idberita',$idberita)->update([
            'judul' => $request->judul,
            'konten' => $request->konten,
            'link_berita' => $request->link_berita,
            'updated_at' => now()
            ]);
        return redirect('/admin/berita');
    }
    public function beritadelete($idberita)
    {
        $beritaold = DB::table('berita')->where('idberita',$idberita)->get()->first();
        if(isset($beritaold->path_gambar)){
            unlink(public_path().'/berita/'.$beritaold->path_gambar);
        }
        DB::table('berita')->where('idberita',$idberita)->delete();
        return redirect('/admin/berita');
    }
    
}
