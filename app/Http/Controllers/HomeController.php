<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        
        if ($user->is_admin) {
            return redirect('/admin');
        }
        return view('dashboard', ['user' => $user->name]);
    }

    public function landingpage(){
        $databerita = Berita::get();
        $datauser = Auth::user();                
        // echo $databerita;
        if($datauser){
            return view('welcome', ['databerita' => $databerita, 'datauser' => $datauser]);            
        }
        else{
            return view('welcome', ['databerita' => $databerita]);
        }                        
    }

    // public function beritadetail(Request $request){
    //     $judulurl = $request->judul;
    //     $judulclean = str_replace("-", " ", $judulurl);

    //     $detailberita =  Berita::where('judul', $judulclean)->get()->first();
    //     return view('beritadetail', ['detailberita' => $detailberita]);
    // }
}
