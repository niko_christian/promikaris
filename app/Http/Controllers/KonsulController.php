<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use DateTime;
use DateTimeZone;
use App\Konsultasi;
use auth;

class KonsulController extends Controller
{
    public function index(){
        $user = Auth::user();                
        $userid = $user->id;
        //$data = Konsultasi::find($userid);
        $data = DB::table('konsultasi')->where('userid', $userid)->get();
        //$data = Konsultasi::get();
        return view('homekonsul_dashboard', ['data' => $data]);
    }
    public function adminindex(){                
        $data = Konsultasi::get();
        return view('adminkonsul_dashboard', ['data' => $data]);
    }
    public function konsultambah()
    {
        return view('konsul_hometambah');
    }
    public function konsulstore(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'alamat' => 'required',
            'gambarkabeltiang' => 'file|image|mimes:jpeg,png,jpg|max:2048',
            'gambarkabelflexible' => 'file|image|mimes:jpeg,png,jpg|max:2048',
            'gambarkabelstopkontak' => 'file|image|mimes:jpeg,png,jpg|max:2048',
            'gambarkabelolor' => 'file|image|mimes:jpeg,png,jpg|max:2048',
            'gambarsteker' => 'file|image|mimes:jpeg,png,jpg|max:2048',
            'gambarsaklar' => 'file|image|mimes:jpeg,png,jpg|max:2048',
            'gambarkabelmeteran' => 'file|image|mimes:jpeg,png,jpg|max:2048',
            'gambarpanelbagi' => 'file|image|mimes:jpeg,png,jpg|max:2048',
            'gambarstopkontaktempel' => 'file|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $kabeltiang='';
        if ($request->file('gambarkabeltiang') != "") {
       
        $filekabeltiang = $request->file('gambarkabeltiang');

        $kabeltiang = $request->judul.'kabeltiang' .time(). '.' . $filekabeltiang->getClientOriginalExtension();

        $filekabeltiang->move(public_path() . '/konsul/', $kabeltiang);
        }
       // echo $kabeltiang;

        $kabelflexible='';
        if ($request->file('gambarkabelflexible') != "") {
       
        $filekabelflexible = $request->file('gambarkabelflexible');

        $kabelflexible = $request->judul.'kabelflexible' .time(). '.' . $filekabelflexible->getClientOriginalExtension();

        $filekabelflexible->move(public_path() . '/konsul/', $kabelflexible);
        }
       // echo $kabelflexible;

        $stopkontak='';
        if ($request->file('gambarstopkontak') != "") {
       
        $filestopkontak = $request->file('gambarstopkontak');

        $stopkontak = $request->judul.'stopkontak' .time(). '.' . $filestopkontak->getClientOriginalExtension();

        $filestopkontak->move(public_path() . '/konsul/', $stopkontak);
        }
       // echo $stopkontak;

        $kabelolor='';
        if ($request->file('gambarkabelolor') != "") {
       
        $filekabelolor = $request->file('gambarkabelolor');

        $kabelolor = $request->judul.'kabelolor' .time(). '.' . $filekabelolor->getClientOriginalExtension();

        $filekabelolor->move(public_path() . '/konsul/', $kabelolor);
        }
       // echo $kabelolor;

        $steker='';
        if ($request->file('gambarsteker') != "") {
       
        $filesteker = $request->file('gambarsteker');

        $steker = $request->judul.'steker' .time(). '.' . $filesteker->getClientOriginalExtension();

        $filesteker->move(public_path() . '/konsul/', $steker);
        }
       // echo $steker;

        $saklar='';
        if ($request->file('gambarsaklar') != "") {
       
        $filesaklar = $request->file('gambarsaklar');

        $saklar = $request->judul.'saklar' .time(). '.' . $filesaklar->getClientOriginalExtension();

        $filesaklar->move(public_path() . '/konsul/', $saklar);
        }
       // echo $saklar;

        $kabelmeteran='';
        if ($request->file('gambarkabelmeteran') != "") {
       
        $filekabelmeteran = $request->file('gambarkabelmeteran');

        $kabelmeteran = $request->judul.'kabelmeteran' .time(). '.' . $filekabelmeteran->getClientOriginalExtension();

        $filekabelmeteran->move(public_path() . '/konsul/', $kabelmeteran);
        }
       // echo $kabelmeteran;

        $panelbagi='';
        if ($request->file('gambarpanelbagi') != "") {
       
        $filepanelbagi = $request->file('gambarpanelbagi');

        $panelbagi = $request->judul.'panelbagi' .time(). '.' . $filepanelbagi->getClientOriginalExtension();

        $filepanelbagi->move(public_path() . '/konsul/', $panelbagi);
        }
       // echo $panelbagi;


        $stopkontaktempel='';
        if ($request->file('gambarstopkontaktempel') != "") {
       
        $filestopkontaktempel = $request->file('gambarstopkontaktempel');

        $stopkontaktempel = $request->judul.'stopkontaktempel' .time(). '.' . $filestopkontaktempel->getClientOriginalExtension();

        $filestopkontaktempel->move(public_path() . '/konsul/', $stopkontaktempel);
        }
       // echo $stopkontaktempel;
        echo $request->nama;
        $user = Auth::user();                
        $userid = $user->id;
        Konsultasi::create([
            'kabeltiang' => $kabeltiang,
            'userid' => $userid,
            'kabelmeteran' => $kabelmeteran,
            'panelbagi' => $panelbagi,
            'stopkontaktempel' => $stopkontaktempel,
            'kabelflexible' => $kabelflexible,
            'stopkontak' => $stopkontak,
            'kabelolor' => $kabelolor,
            'steker' => $steker,
            'saklar' => $saklar,
            'komentar' => null,
            'keluhan' => $request->keluhan,
            'nama' => $request->nama,
            'alamat' => $request->alamat,
        ]);
        return redirect('/home/konsul');
    }
    public function konsuledit($id)
    {
        //echo $id;
        $e = Konsultasi::find($id);
       
        return view('konsul_homeedit', ['e' => $e]);
    }
    public function adminkonsuledit($id)
    {
        //echo $id;
        $e = Konsultasi::find($id);
       
        return view('konsul_adminedit', ['e' => $e]);
    }
    public function konsulupdate($id, Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'alamat' => 'required',
        ]);

        $konsul = Konsultasi::find($id);
        $konsul->nama = $request->nama;
        $konsul->alamat = $request->alamat;
        $konsul->keluhan = $request->keluhan;
        $konsul->updated_at= now();
        
        $kabeltiang='';
        if ($request->file('gambarkabeltiang') != "") {
       
        $filekabeltiang = $request->file('gambarkabeltiang');

        $kabeltiang = $request->judul.'kabeltiang' .time(). '.' . $filekabeltiang->getClientOriginalExtension();

        $filekabeltiang->move(public_path() . '/konsul/', $kabeltiang);
        $konsul->kabeltiang = $kabeltiang;
        }
       // echo $kabeltiang;

        $kabelflexible='';
        if ($request->file('gambarkabelflexible') != "") {
       
        $filekabelflexible = $request->file('gambarkabelflexible');

        $kabelflexible = $request->judul.'kabelflexible' .time(). '.' . $filekabelflexible->getClientOriginalExtension();

        $filekabelflexible->move(public_path() . '/konsul/', $kabelflexible);
        $konsul->kabelflexible = $kabelflexible;
        }
       // echo $kabelflexible;

        $stopkontak='';
        if ($request->file('gambarstopkontak') != "") {
       
        $filestopkontak = $request->file('gambarstopkontak');

        $stopkontak = $request->judul.'stopkontak' .time(). '.' . $filestopkontak->getClientOriginalExtension();

        $filestopkontak->move(public_path() . '/konsul/', $stopkontak);
        $konsul->stopkontak = $stopkontak;
        }
       // echo $stopkontak;

        $kabelolor='';
        if ($request->file('gambarkabelolor') != "") {
       
        $filekabelolor = $request->file('gambarkabelolor');

        $kabelolor = $request->judul.'kabelolor' .time(). '.' . $filekabelolor->getClientOriginalExtension();

        $filekabelolor->move(public_path() . '/konsul/', $kabelolor);
        $konsul->kabelolor = $kabelolor;
        }
       // echo $kabelolor;

        $steker='';
        if ($request->file('gambarsteker') != "") {
       
        $filesteker = $request->file('gambarsteker');

        $steker = $request->judul.'steker' .time(). '.' . $filesteker->getClientOriginalExtension();

        $filesteker->move(public_path() . '/konsul/', $steker);
        $konsul->steker = $steker;
        }
       // echo $steker;

        $saklar='';
        if ($request->file('gambarsaklar') != "") {
       
        $filesaklar = $request->file('gambarsaklar');

        $saklar = $request->judul.'saklar' .time(). '.' . $filesaklar->getClientOriginalExtension();

        $filesaklar->move(public_path() . '/konsul/', $saklar);
        $konsul->saklar = $saklar;
        }
       // echo $saklar;

        $kabelmeteran='';
        if ($request->file('gambarkabelmeteran') != "") {
       
        $filekabelmeteran = $request->file('gambarkabelmeteran');

        $kabelmeteran = $request->judul.'kabelmeteran' .time(). '.' . $filekabelmeteran->getClientOriginalExtension();

        $filekabelmeteran->move(public_path() . '/konsul/', $kabelmeteran);
        $konsul->kabelmeteran = $kabelmeteran;
        }
       // echo $kabelmeteran;

        $panelbagi='';
        if ($request->file('gambarpanelbagi') != "") {
       
        $filepanelbagi = $request->file('gambarpanelbagi');

        $panelbagi = $request->judul.'panelbagi' .time(). '.' . $filepanelbagi->getClientOriginalExtension();

        $filepanelbagi->move(public_path() . '/konsul/', $panelbagi);
        $konsul->panelbagi = $panelbagi;
        }
       echo $panelbagi;


        $stopkontaktempel='';
        if ($request->file('gambarstopkontaktempel') != "") {
       
        $filestopkontaktempel = $request->file('gambarstopkontaktempel');

        $stopkontaktempel = $request->judul.'stopkontaktempel' .time(). '.' . $filestopkontaktempel->getClientOriginalExtension();

        $filestopkontaktempel->move(public_path() . '/konsul/', $stopkontaktempel);
        $konsul->stopkontaktempel = $stopkontaktempel;
        }
       // echo $stopkontaktempel;
       $konsul->save();
       return redirect('/home/konsul');
    }
    public function adminkonsulupdate($id, Request $request)
    {
        $this->validate($request, [
            'komentar' => 'required',
        ]);

        $konsul = Konsultasi::find($id);
        $konsul->komentar = $request->komentar;
        $konsul->updated_at= now();
       
       $konsul->save();
       return redirect('/admin/konsul');
    }
    public function konsuldelete($id)
    {
        $konsultasi = Konsultasi::find($id);
        $konsultasi->delete();
        return redirect('/home/konsul');
    }
    public function adminkonsuldelete($id)
    {
        $konsultasi = Konsultasi::find($id);
        $konsultasi->delete();
        return redirect('/admin/konsul');
    }
}
