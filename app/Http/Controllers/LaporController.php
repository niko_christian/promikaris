<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use DateTime;
use DateTimeZone;
use App\Lapor;
use auth;
use Laracsv\Export;
class LaporController extends Controller
{    
    
    public function downloadCsv() {
        $lapor = Lapor::select('lapor.*')
        ->get();

        $csv = new Export();
        $csv->build($lapor, [
            'nama',
            'kota',
            'stopkontakgosong',
            'kabeltidakrapi',
            'ukurankabelsalah',
            'kabeltidakterjaga',
            'kabeltua',
            'stopkontakbertumpuk',
        ])->download('lapor'.date('Y-m-d').'.csv');
    }


    public function index(){
        $user = Auth::user();                
        $userid = $user->id;
        //echo $userid;
        //$data = Konsultasi::find($userid);
        $data = DB::table('lapor')->where('userid', $userid)->get();
        //var_dump($data);
        //$data = Konsultasi::get();
        return view('homelapor_dashboard', ['data' => $data]);
    }
    public function adminindex(){                
        $data = Lapor::get();
        return view('adminlapor_dashboard', ['data' => $data]);
    }
    public function laportambah()
    {
        return view('lapor_hometambah');
    }
    public function laporstore(Request $request)
    {
        echo 'asa';
       // echo $request->input('stopkontakgosong');
        $this->validate($request, [
            'nama' => 'required',
            'kota' => 'required',
            'stopkontakgosong' => 'required',
            'kabeltidakrapi' => 'required',
            'ukurankabelsalah' => 'required',
            'kabeltidakterjaga' => 'required',
            'kabeltua' => 'required',
            'stopkontakbertumpuk' => 'required',
 
        ]);

        Lapor::create([
            'userid' => $userId = Auth::user()->id,
            'nama' => $request->nama,
            'kota' => $request->kota,
            'stopkontakgosong' => $request->input('stopkontakgosong'),
            'kabeltidakrapi' => $request->input('kabeltidakrapi'),
            'ukurankabelsalah' => $request->input('ukurankabelsalah'),
            'kabeltidakterjaga' => $request->input('kabeltidakterjaga'),
            'kabeltua' => $request->input('kabeltua'),
            'stopkontakbertumpuk' => $request->input('stopkontakbertumpuk'),
        ]);
        return redirect('/home/lapor');
    }
    public function laporedit($id)
    {
        //echo $idberita;
        $data = DB::table('lapor')->where('id',$id)->get();
        return view('lapor_homeedit', ['data' => $data ]);
    }
    public function adminlaporedit($id)
    {
        //echo $idberita;
        $data = DB::table('lapor')->where('id',$id)->get();
        return view('lapor_adminedit', ['data' => $data ]);
    }
    public function laporupdate($id, Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'kota' => 'required',
            'stopkontakgosong' => 'required',
            'kabeltidakrapi' => 'required',
            'ukurankabelsalah' => 'required',
            'kabeltidakterjaga' => 'required',
            'kabeltua' => 'required',
            'stopkontakbertumpuk' => 'required',
        ]);
        echo $id;
        
        DB::table('lapor')->where('id',$id)->update([
            'nama' => $request->nama,
            'kota' => $request->kota,
            'stopkontakgosong' => $request->input('stopkontakgosong'),
            'kabeltidakrapi' => $request->input('kabeltidakrapi'),
            'ukurankabelsalah' => $request->input('ukurankabelsalah'),
            'kabeltidakterjaga' => $request->input('kabeltidakterjaga'),
            'kabeltua' => $request->input('kabeltua'),
            'stopkontakbertumpuk' => $request->input('stopkontakbertumpuk'),
            'updated_at' => now()
            ]);
        return redirect('/home/lapor');
    }
    public function lapordelete($id)
    {
        DB::table('lapor')->where('id',$id)->delete();
        return redirect('/home/lapor');
    }
    public function adminlapordelete($id)
    {
        DB::table('lapor')->where('id',$id)->delete();
        return redirect('/admin/lapor');
    }
}
