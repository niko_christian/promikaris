<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Konsultasi extends Model
{
    protected $table = "konsultasi";
    public function users()
    {
    	return $this->belongsTo('App\User');
    }
    protected $fillable = ['userid','kabeltiang','kabelmeteran','panelbagi','stopkontaktempel',
    'kabelflexible','stopkontak','kabelolor','steker','saklar','komentar','nama','alamat','keluhan'];

  
}
