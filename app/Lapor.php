<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lapor extends Model
{
    protected $table = "lapor";
    public function users()
    {
    	return $this->belongsTo('App\User');
    }
    protected $fillable = ['userid','nama','kota','stopkontakgosong','kabeltidakrapi',
    'ukurankabelsalah','kabeltidakterjaga','kabeltua','stopkontakbertumpuk'];

} 
