<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKonsultasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('konsultasi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('userid');
            $table->string('nama');
            $table->string('alamat');
            $table->string('kabeltiang');
            $table->string('kabelmeteran');
            $table->string('panelbagi');
            $table->string('stopkontaktempel');
            $table->string('kabelflexible');
            $table->string('stopkontak');
            $table->string('kabelolor');
            $table->string('steker');
            $table->string('saklar');
            $table->text('keluhan')->nullable();
            
            $table->text('komentar')->nullable();

            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('konsultasi');
    }
}
