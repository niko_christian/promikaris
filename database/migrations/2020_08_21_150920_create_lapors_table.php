<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLaporsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lapor', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('userid');
            $table->string('nama');
            $table->string('kota');
            $table->string('stopkontakgosong');
            $table->string('kabeltidakrapi');
            $table->string('ukurankabelsalah');
            $table->string('kabeltidakterjaga');
            $table->string('kabeltua');
            $table->string('stopkontakbertumpuk');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lapor');
    }
}
