@extends('layouts.dashboard')

@section('content')

<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Dashboard </h1>
    <h4 class="h5 mb-0 text-gray-800">Just Click to go to dashboard</h4>
</div>


<!-- Content Row -->
<div class="row">

    <!-- Berita -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-warning shadow h-100 py-2" onclick="location.href='/admin/berita'">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col">
                        <div class="font-weight-bold text-warning text-uppercase mb-1">
                            Berita
                        </div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                           Untuk Atur Artikel
                        </div>
                        <a href="/admin/berita" class="btn btn-warning btn-icon-split btn-block mt-4 mb-0">
                            <span class="text">lihat dashboard</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Konsul -->
  <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-info shadow h-100 py-2" onclick="location.href='/admin/konsul'">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col">
                        <div class="font-weight-bold text-info text-uppercase mb-1">
                            Konsul
                        </div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                           Untuk Konsultasi
                        </div>
                        <a href="/admin/konsul" class="btn btn-info btn-icon-split btn-block mt-4 mb-0">
                            <span class="text">lihat dashboard</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- lapor -->
  <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-dark shadow h-100 py-2" onclick="location.href='/home/lapor'">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col">
                        <div class="font-weight-bold text-dark text-uppercase mb-1">
                            Lapor
                        </div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                           Untuk Laporan
                        </div>
                        <a href="/admin/lapor" class="btn btn-dark btn-icon-split btn-block mt-4 mb-0">
                            <span class="text">lihat dashboard</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Laporan', 'Jumlah'],
          ['stop kontak gosong ',  {{ $chart[0] }} ],
          ['kabel tidak rapi ',      {{ $chart[1] }} ],
          ['ukuran kabel salah', {{ $chart[2] }} ],
          ['kabel tidak terjaga baik', {{ $chart[3] }} ],
          ['kabel sudah tua',    {{ $chart[4] }} ],
          ['stop kontak bertumpuk',    {{ $chart[5] }} ]
        ]);

        var options = {
          title: 'Persentase Laporan'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>
     <div id="piechart" style="width: 100%; height: 500px;"></div>
     


    @endsection