@extends('layouts.dashboard')

@section('content')

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">List Berita</h1>
</div>


<div class="table-responsive">
    <table class="table table-bordered table-striped display nowrap" style="width:100%" id="dataTable">
        <thead>
            <tr>
                <th>Id Berita</th>
                <th>Judul</th>
                <th>Konten</th>
                <th>Link Berita</th>
                <th>Tanggal</th>
                <th>Gambar</th>
                <th>OPSI</th>

            </tr>
        </thead>
        <tbody>
            @foreach($data as $e)
            <tr>
                <td>{{ $e->idberita }}</td>
                <td>{{ $e->judul }}</td>
                <td>{{  $e->konten  }}</td>
                <td>{{ $e->link_berita }}</td>
                <td>{{ $e->updated_at }}</td>
                
                <td>
                    @if ($e->path_gambar)
                    <a href="{{url( '/berita/'. $e->path_gambar)}}">{{$e->path_gambar}}</a>
                    @endif
                </td>


                <td>

                    <a href="/admin/berita/edit/{{ $e->idberita }}" class="btn btn-warning">Edit</a>
                    <a href="/admin/berita/hapus/{{ $e->idberita }}" class="btn btn-danger">Hapus</a>
                
                </td>


            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection