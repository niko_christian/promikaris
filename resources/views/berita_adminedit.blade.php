@extends('layouts.dashboard')


@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Edit Data Berita</h1>
</div>

<div class="container">


    @foreach($data as $e)
    <form method="post" action="/admin/berita/update/{{ $e->idberita }}" enctype="multipart/form-data">

        {{ csrf_field() }}
        {{ method_field('PUT') }}
        
        <div class="form-group">
            <label>Judul</label>
             <input type="text" name="judul" class="form-control" placeholder="Judul .." value="  {{ $e->judul }} " >
          
            @if($errors->has('judul'))
            <div class="text-danger">
                {{ $errors->first('judul')}}
            </div>
            @endif

        </div>
        <div class="form-group">
            <label>Konten</label>
            <textarea name="konten" class="form-control"
                placeholder="konten .."> {{ $e->konten }} </textarea>

            @if($errors->has('konten'))
            <div class="text-danger">
                {{ $errors->first('konten')}}
            </div>
            @endif

        </div>

        <div class="form-group">
            <label>Link Berita</label>
            <input name="link_berita" class="form-control"
                placeholder="Link Berita .." value="{{ $e->link_berita }}">

            @if($errors->has('link_berita'))
            <div class="text-danger">
                {{ $errors->first('link_berita')}}
            </div>
            @endif
            
        </div>


        <div class="form-group">
            <label>
                <b>Gambar Berita</b>
            </label>

            @if($e->path_gambar)
            <div class="text-success">File Sudah Diupload
                <a href="{{ url('/berita/'.$e->path_gambar) }}">
                    lihat file</a>
            </div>
            @endif
            <input type="file" name="gambarberita">

        </div>


        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Simpan">
        </div>

    </form>
    @endforeach

</div>
@endsection