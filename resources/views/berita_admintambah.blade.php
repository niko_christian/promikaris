@extends('layouts.dashboard')

@section('content')

<div class="container">

<h1 class="h3 mb-0 text-gray-800">Tambah Berita</h1></br>
    <form method="post" action="/admin/berita/store" enctype="multipart/form-data">

        {{ csrf_field() }}

        <div class="form-group">
            <label>Judul</label>
            <input type="text" name="judul" class="form-control" placeholder="Judul ..">

            @if($errors->has('judul'))
            <div class="text-danger">
                {{ $errors->first('judul')}}
            </div>
            @endif

        </div>

        <div class="form-group">
            <label>Konten</label>
            <textarea id="konten" name="konten" rows="4" cols="50" class="form-control" placeholder="konten .."></textarea>
            @if($errors->has('konten'))
            <div class="text-danger">
                {{ $errors->first('konten')}}
            </div>
            @endif
            <small>*Disarankan 15 kata</small>
        </div>

        <div class="form-group">
            <label>Link Berita</label>
            <input name="link_berita" class="form-control"
                placeholder="Link Berita ..">

            @if($errors->has('link_berita'))
            <div class="text-danger">
                {{ $errors->first('link_berita')}}
            </div>
            @endif
            
        </div>    

        <div class="form-group">
            <div class="custom-file">
                <label>Gambar Berita</label>
                <input type="file" class="form-control-file" name="gambarberita">
            </div>
        </div>

       

        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Simpan">
        </div>


    </form>

</div>

@endsection