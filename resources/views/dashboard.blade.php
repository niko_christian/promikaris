@extends('layouts.dashboard')

@section('content')


<!-- Page Heading -->

<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
  <h4 class="h5 mb-0 text-gray-800">Click to go to dashboard</h4>
</div>

@if($errors->has('failed'))
<div class="alert alert-danger" role="alert">
  {{ $errors->first('failed')}}
</div>
@endif

<!-- Content Row -->
<div class="row">
 
  
  <!-- Konsul -->
  <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-info shadow h-100 py-2" onclick="location.href='/home/konsul'">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col">
                        <div class="font-weight-bold text-info text-uppercase mb-1">
                            Konsul
                        </div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                           Untuk Konsultasi
                        </div>
                        <a href="/home/konsul" class="btn btn-info btn-icon-split btn-block mt-4 mb-0">
                            <span class="text">lihat dashboard</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- lapor -->
  <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-dark shadow h-100 py-2" onclick="location.href='/home/lapor'">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col">
                        <div class="font-weight-bold text-dark text-uppercase mb-1">
                            Lapor
                        </div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                           Untuk Laporan
                        </div>
                        <a href="/home/lapor" class="btn btn-dark btn-icon-split btn-block mt-4 mb-0">
                            <span class="text">lihat dashboard</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

  

  @endsection