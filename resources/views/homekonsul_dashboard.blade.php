@extends('layouts.dashboard')

@section('content')

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">List Konsul</h1>
</div>


<div class="table-responsive">
    <table class="table table-bordered table-striped display nowrap" style="width:100%" id="dataTable">
        <thead>
            <tr>
                <th>Id</th>
                
                <th>Nama</th>
                <th>Alamat</th>
                <th>Komentar</th>
                <th>Keluhan</th>
                <th>TanggalUpdate</th>
                <th>OPSI</th>

            </tr>
        </thead>
        <tbody>
            @foreach($data as $e)
            <tr>
                <td>{{ $e->id }}</td>
             
                <td>{{ $e->nama }}</td>
                <td>{{ $e->alamat }}</td>
                <td>{{ $e->komentar }}</td>
                <td>{{ $e->keluhan }}</td>
                <td>{{ $e->updated_at }}</td>
          

                <td>

                    <a href="/home/konsul/edit/{{ $e->id }}" class="btn btn-warning">Edit</a>
                    <a href="/home/konsul/hapus/{{ $e->id }}" class="btn btn-danger">Hapus</a>
                
                </td>


            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection