@extends('layouts.dashboard')


@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Edit Data Konsul</h1>
</div>

<div class="container">


    
    <form method="post" action="/admin/konsul/update/{{ $e->id }}" enctype="multipart/form-data">

        {{ csrf_field() }}
        {{ method_field('PUT') }}
        
        <div class="form-group" >
            <label>nama</label>
             <input type="text" name="nama" class="form-control" disabled="disabled" placeholder="nama .." value="  {{ $e->nama }} " >
          
            @if($errors->has('nama'))
            <div class="text-danger">
                {{ $errors->first('nama')}}
            </div>
            @endif

        </div>
        <div class="form-group">
            <label>alamat</label>
            <textarea name="alamat" class="form-control " disabled="disabled"
                placeholder="alamat .."> {{ $e->alamat }} </textarea>

            @if($errors->has('alamat'))
            <div class="text-danger">
                {{ $errors->first('alamat')}}
            </div>
            @endif

        </div>
        <div class="form-group">
            <label>keluhan</label>
            <textarea name="keluhan" class="form-control" disabled="disabled"
                placeholder="keluhan .."> {{ $e->keluhan }} </textarea>

            @if($errors->has('keluhan'))
            <div class="text-danger">
                {{ $errors->first('keluhan')}}
            </div>
            @endif

        </div>


        <div class="form-group">
            <label>
                <b>Gambar Kabel Tiang</b>
            </label>

            @if($e->kabeltiang)
            <div class="text-success">File Sudah Diupload
                <a href="{{ url('/konsul/'.$e->kabeltiang) }}">
                    lihat file</a>
            </div>
            @endif
           

        </div>
        <div class="form-group">
            <label>
                <b>Gambar Kabel Meteran</b>
            </label>

            @if($e->kabelmeteran)
            <div class="text-success">File Sudah Diupload
                <a href="{{ url('/konsul/'.$e->kabelmeteran) }}">
                    lihat file</a>
            </div>
            @endif
    

        </div>
        <div class="form-group">
            <label>
                <b>Gambar Panel Bagi</b>
            </label>

            @if($e->panelbagi)
            <div class="text-success">File Sudah Diupload
                <a href="{{ url('/konsul/'.$e->panelbagi) }}">
                    lihat file</a>
            </div>
            @endif
   

        </div>
        <div class="form-group">
            <label>
                <b>Gambar Stopkontak Tempel</b>
            </label>

            @if($e->kabeltiang)
            <div class="text-success">File Sudah Diupload
                <a href="{{ url('/konsul/'.$e->kabeltiang) }}">
                    lihat file</a>
            </div>
            @endif


        </div>
        <div class="form-group">
            <label>
                <b>Gambar Kabel Flexible</b>
            </label>

            @if($e->kabelflexible)
            <div class="text-success">File Sudah Diupload
                <a href="{{ url('/konsul/'.$e->kabelflexible) }}">
                    lihat file</a>
            </div>
            @endif
         

        </div>
       
        
        <div class="form-group">
            <label>
                <b>Gambar Stop Kontak</b>
            </label>

            @if($e->stopkontak)
            <div class="text-success">File Sudah Diupload
                <a href="{{ url('/konsul/'.$e->stopkontak) }}">
                    lihat file</a>
            </div>
            @endif
            

        </div>
        <div class="form-group">
            <label>
                <b>Gambar Kabel Olor</b>
            </label>

            @if($e->kabelolor)
            <div class="text-success">File Sudah Diupload
                <a href="{{ url('/konsul/'.$e->kabelolor) }}">
                    lihat file</a>
            </div>
            @endif
            

        </div>
        <div class="form-group">
            <label>
                <b>Gambar Steker</b>
            </label>

            @if($e->steker)
            <div class="text-success">File Sudah Diupload
                <a href="{{ url('/konsul/'.$e->steker) }}">
                    lihat file</a>
            </div>
            @endif
            

        </div>

        <div class="form-group">
            <label>
                <b>Gambar Saklar</b>
            </label>

            @if($e->saklar)
            <div class="text-success">File Sudah Diupload
                <a href="{{ url('/konsul/'.$e->saklar) }}">
                    lihat file</a>
            </div>
            @endif
         

        </div>
        <div class="form-group">
            <label>komentar</label>
            <textarea name="komentar" class="form-control" 
                placeholder="komentar .."> {{ $e->komentar }} </textarea>

            @if($errors->has('komentar'))
            <div class="text-danger">
                {{ $errors->first('komentar')}}
            </div>
            @endif

        </div>

        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Simpan">
        </div>

    </form>


</div>
@endsection