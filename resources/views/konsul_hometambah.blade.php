@extends('layouts.dashboard')

@section('content')

<div class="container">

<h1 class="h3 mb-0 text-gray-800">Tambah Konsul</h1></br>

    <form method="post" action="/home/konsul/store" enctype="multipart/form-data">

        {{ csrf_field() }}

        <div class="form-group">
            <label>nama</label>
            <input type="text" name="nama" class="form-control" placeholder="nama ..">

            @if($errors->has('nama'))
            <div class="text-danger">
                {{ $errors->first('nama')}}
            </div>
            @endif

        </div>

        <div class="form-group">
            <label>alamat</label>
            <textarea id="alamat" name="alamat" rows="4" cols="50" class="form-control" placeholder="alamat .."></textarea>
            @if($errors->has('alamat'))
            <div class="text-danger">
                {{ $errors->first('alamat')}}
            </div>
            @endif

        </div>
        <div class="form-group">
            <label>keluhan</label>
            <textarea id="keluhan" name="keluhan" rows="4" cols="50" class="form-control" placeholder="keluhan .."></textarea>
            @if($errors->has('keluhan'))
            <div class="text-danger">
                {{ $errors->first('keluhan')}}
            </div>
            @endif

        </div>
    


        <div class="form-group">
            <div class="custom-file">
                <label>Gambar Kabel Tiang</label>
                <input type="file" class="form-control-file" name="gambarkabeltiang">
            </div>
        </div>
        <div class="form-group">
            <div class="custom-file">
                <label>Gambar Kabel Meteran</label>
                <input type="file" class="form-control-file" name="gambarkabelmeteran">
            </div>
        </div>
        <div class="form-group">
            <div class="custom-file">
                <label>Gambar Panel Bagi</label>
                <input type="file" class="form-control-file" name="gambarpanelbagi">
            </div>
        </div> 
        <div class="form-group">
            <div class="custom-file">
                <label>Gambar Stopkontak tempel</label>
                <input type="file" class="form-control-file" name="gambarstopkontaktempel">
            </div>
        </div> 
        <div class="form-group">
            <div class="custom-file">
                <label>Gambar Kabel Flexible</label>
                <input type="file" class="form-control-file" name="gambarkabelflexible">
            </div>
        </div> 
        <div class="form-group">
            <div class="custom-file">
                <label>Gambar Stop Kontak</label>
                <input type="file" class="form-control-file" name="gambarstopkontak">
            </div>
        </div> 
        <div class="form-group">
            <div class="custom-file">
                <label>Gambar Kabel Olor</label>
                <input type="file" class="form-control-file" name="gambarkabelolor">
            </div>
        </div>
        
        <div class="form-group">
            <div class="custom-file">
                <label>Gambar Steker</label>
                <input type="file" class="form-control-file" name="gambarsteker">
            </div>
        </div>
        <div class="form-group">
            <div class="custom-file">
                <label>Gambar Saklar</label>
                <input type="file" class="form-control-file" name="gambarsaklar">
            </div>
        </div>
       

        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Simpan">
        </div>


    </form>

</div>
@endsection