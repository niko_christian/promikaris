@extends('layouts.dashboard')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Edit Data Lapor</h1>
</div>


<div class="container">

    <form action="/admin/lapor/" enctype="multipart/form-data">

        {{ csrf_field() }}
     

        <div class="form-group">
            <label>Nama</label>
            <textarea name="nama" class="form-control" disabled="disabled"
                placeholder=" Nama tim .."> {{ $data[0]->nama }} </textarea>

            @if($errors->has('nama'))
            <div class="text-danger">
                {{ $errors->first('nama')}}
            </div>
            @endif

        </div>
    


        <div class="form-group">
            <label>kota</label>
            <select name="kota" class="form-control" disabled="disabled">
                <option value="Banyuwangi" {{$data[0]->kota === 'Banyuwangi' ? 'selected' : null}}>
                    Banyuwangi</option>
                <option value="Probolinggo" {{$data[0]->kota === 'Probolinggo' ? 'selected' : null}}>
                    Probolinggo</option>
                <option value="Malang" {{$data[0]->kota === 'Malang' ? 'selected' : null}}>Malang</option>
                <option value="Jember" {{$data[0]->kota === 'Jember' ? 'selected' : null}}>Jember</option>
                <option value="Solo" {{$data[0]->kota === 'Solo' ? 'selected' : null}}>Solo</option>
                <option value="Madura" {{$data[0]->kota === 'Madura' ? 'selected' : null}}>Madura</option>
                <option value="Gresik" {{$data[0]->kota === 'Gresik' ? 'selected' : null}}>Gresik</option>
                <option value="Bali" {{$data[0]->kota === 'Bali' ? 'selected' : null}}>Bali</option>
                <option value="Surabaya" {{$data[0]->kota === 'Surabaya' ? 'selected' : null}}>Surabaya
                </option>
                <option value="Sidoarjo" {{$data[0]->kota === 'Sidoarjo' ? 'selected' : null}}>Sidoarjo
                </option>
                <option value="Madiun" {{$data[0]->kota === 'Madiun' ? 'selected' : null}}>Madiun</option>
                <option value="Mojokerto" {{$data[0]->kota === 'Mojokerto' ? 'selected' : null}}>Mojokerto
                </option>
                <option value="Jabodetabek" {{$data[0]->kota === 'Jabodetabek' ? 'selected' : null}}>
                    Jabodetabek</option>
                <option value="Kalimantan" {{$data[0]->kota === 'Kalimantan' ? 'selected' : null}}>
                    Kalimantan</option>
                <option value="Kediri" {{$data[0]->kota === 'Kediri' ? 'selected' : null}}>Kediri</option>
                <option value="Semarang" {{$data[0]->kota === 'Semarang' ? 'selected' : null}}>Semarang
                </option>
                <option value="Tuban" {{$data[0]->kota === 'Tuban' ? 'selected' : null}}>Tuban</option>
                <option value="Lumajang" {{$data[0]->kota === 'Lumajang' ? 'selected' : null}}>Lumajang
                </option>
            </select>

            @if($errors->has('kota'))
            <div class="text-danger">
                {{ $errors->first('kota')}}
            </div>
            @endif

        </div>
        <div class="form-group">
            <label>Stop kontak sudah gosong dan tidak berSNI ?</label>
            <select name="stopkontakgosong" class="form-control" disabled="disabled"> 
                <option value="Ya" {{$data[0]->stopkontakgosong === 'Ya' ? 'selected' : null}}>
                    Ya</option>
                <option value="Tidak" {{$data[0]->stopkontakgosong === 'Tidak' ? 'selected' : null}}>
                    Tidak</option>
               
            </select>

            @if($errors->has('stopkontakgosong'))
            <div class="text-danger">
                {{ $errors->first('stopkontakgosong')}}
            </div>
            @endif

        </div>
        <div class="form-group">
            <label>Sambungan kabel tidak rapi ?</label>
            <select name="kabeltidakrapi" class="form-control" disabled="disabled">
                <option value="Ya" {{$data[0]->kabeltidakrapi === 'Ya' ? 'selected' : null}}>
                    Ya</option>
                <option value="Tidak" {{$data[0]->kabeltidakrapi === 'Tidak' ? 'selected' : null}}>
                    Tidak</option>
               
            </select>

            @if($errors->has('kabeltidakrapi'))
            <div class="text-danger">
                {{ $errors->first('kabeltidakrapi')}}
            </div>
            @endif

        </div>
        <div class="form-group">
            <label>Ukuran kabel tidak sesuai dengan daya kabel ?</label>
            <select name="ukurankabelsalah" class="form-control" disabled="disabled">
                <option value="Ya" {{$data[0]->ukurankabelsalah === 'Ya' ? 'selected' : null}}>
                    Ya</option>
                <option value="Tidak" {{$data[0]->ukurankabelsalah === 'Tidak' ? 'selected' : null}}>
                    Tidak</option>
               
            </select>

            @if($errors->has('ukurankabelsalah'))
            <div class="text-danger">
                {{ $errors->first('ukurankabelsalah')}}
            </div>
            @endif

        </div>
        <div class="form-group">
            <label>Kabel tidak terjaga dengan baik (dimakan hewan) ?</label>
            <select name="kabeltidakterjaga" class="form-control" disabled="disabled">
                <option value="Ya" {{$data[0]->kabeltidakterjaga === 'Ya' ? 'selected' : null}}>
                    Ya</option>
                <option value="Tidak" {{$data[0]->kabeltidakterjaga === 'Tidak' ? 'selected' : null}}>
                    Tidak</option>
               
            </select>

            @if($errors->has('kabeltidakterjaga'))
            <div class="text-danger">
                {{ $errors->first('kabeltidakterjaga')}}
            </div>
            @endif

        </div>
        <div class="form-group">
            <label>Umur kabel yang sudah tua (kabel rusak/rapuh) ?</label>
            <select name="kabeltua" class="form-control" disabled="disabled">
                <option value="Ya" {{$data[0]->kabeltua === 'Ya' ? 'selected' : null}}>
                    Ya</option>
                <option value="Tidak" {{$data[0]->kabeltua === 'Tidak' ? 'selected' : null}}>
                    Tidak</option>
               
            </select>

            @if($errors->has('kabeltua'))
            <div class="text-danger">
                {{ $errors->first('kabeltua')}}
            </div>
            @endif

        </div>
        <div class="form-group">
            <label>Pemakaian stopkontak bertumpuk ?</label>
            <select name="stopkontakbertumpuk" class="form-control" disabled="disabled">
                <option value="Ya" {{$data[0]->stopkontakbertumpuk === 'Ya' ? 'selected' : null}}>
                    Ya</option>
                <option value="Tidak" {{$data[0]->stopkontakbertumpuk === 'Tidak' ? 'selected' : null}}>
                    Tidak</option>
               
            </select>

            @if($errors->has('stopkontakbertumpuk'))
            <div class="text-danger">
                {{ $errors->first('stopkontakbertumpuk')}}
            </div>
            @endif

        </div>

        <div class="form-group">
            <input type="submit" class="btn btn-info" value="Back">
        </div>

    </form>

</div>
@endsection
