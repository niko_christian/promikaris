@extends('layouts.dashboard')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Tambah Laporan</h1>
</div>

<div class="container">
    <form method="post" action="/home/lapor/store" enctype="multipart/form-data">

        {{ csrf_field() }}

        <div class="form-group">
            <label>nama</label>
            <input type="text" name="nama" class="form-control" placeholder="Nama Pelapor ..">

            @if($errors->has('nama'))
            <div class="text-danger">
                {{ $errors->first('nama')}}
            </div>
            @endif

        </div>
        

        <div class="form-group">
            <label>kota</label>
            <select name="kota" class="form-control">
                <option value="" selected></option>
                <option value="Banyuwangi">Banyuwangi</option>
                <option value="Probolinggo">Probolinggo</option>
                <option value="Malang">Malang</option>
                <option value="Jember">Jember</option>
                <option value="Solo">Solo</option>
                <option value="Madura">Madura</option>
                <option value="Gresik">Gresik</option>
                <option value="Bali">Bali</option>
                <option value="Surabaya">Surabaya</option>
                <option value="Sidoarjo">Sidoarjo</option>
                <option value="Madiun">Madiun</option>
                <option value="Mojokerto">Mojokerto</option>
                <option value="Jabodetabek">Jabodetabek</option>
                <option value="Kediri">Kediri</option>
                <option value="Semarang">Semarang</option>
                <option value="Tuban">Tuban</option>
                <option value="Lumajang">Lumajang</option>
            </select>


            @if($errors->has('kota'))
            <div class="text-danger">
                {{ $errors->first('kota')}}
            </div>
            @endif

        </div>

        <div class="form-group">
            <label>Stop kontak sudah gosong dan tidak berSNI ?</label>
            <select name="stopkontakgosong" class="form-control">
                <option value="" selected></option>
                <option value="Ya">Ya</option>
                <option value="Tidak">Tidak</option>
            </select>

            @if($errors->has('stopkontakgosong'))
            <div class="text-danger">
                {{ $errors->first('stopkontakgosong')}}
            </div>
            @endif
        </div>

        <div class="form-group">
            <label>Sambungan kabel tidak rapi ?</label>
            <select name="kabeltidakrapi" class="form-control">
                <option value="" selected></option>
                <option value="Ya">Ya</option>
                <option value="Tidak">Tidak</option>
            </select>

            @if($errors->has('kabeltidakrapi'))
            <div class="text-danger">
                {{ $errors->first('kabeltidakrapi')}}
            </div>
            @endif
        </div>


        <div class="form-group">
            <label>Ukuran kabel tidak sesuai dengan daya kabel ?</label>
            <select name="ukurankabelsalah" class="form-control">
                <option value="" selected></option>
                <option value="Ya">Ya</option>
                <option value="Tidak">Tidak</option>
            </select>

            @if($errors->has('ukurankabelsalah '))
            <div class="text-danger">
                {{ $errors->first('ukurankabelsalah ')}}
            </div>
            @endif
        </div>

        <div class="form-group">
            <label>Kabel tidak terjaga dengan baik (dimakan hewan) ?</label>
            <select name="kabeltidakterjaga" class="form-control">
                <option value="" selected></option>
                <option value="Ya">Ya</option>
                <option value="Tidak">Tidak</option>
            </select>

            @if($errors->has('kabeltidakterjaga  '))
            <div class="text-danger">
                {{ $errors->first('kabeltidakterjaga  ')}}
            </div>
            @endif
        </div>

        <div class="form-group">
            <label>Umur kabel yang sudah tua (kabel rusak/rapuh) ?</label>
            <select name="kabeltua" class="form-control">
                <option value="" selected></option>
                <option value="Ya">Ya</option>
                <option value="Tidak">Tidak</option>
            </select>

            @if($errors->has('kabeltua'))
            <div class="text-danger">
                {{ $errors->first('kabeltua')}}
            </div>
            @endif
        </div>


        <div class="form-group">
            <label>Pemakaian stopkontak bertumpuk ?</label>
            <select name="stopkontakbertumpuk" class="form-control">
                <option value="" selected></option>
                <option value="Ya">Ya</option>
                <option value="Tidak">Tidak</option>
            </select>

            @if($errors->has('stopkontakbertumpuk'))
            <div class="text-danger">
                {{ $errors->first('stopkontakbertumpuk')}}
            </div>
            @endif
        </div>
        
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Simpan">
        </div>
        

    </form>

</div>
@endsection