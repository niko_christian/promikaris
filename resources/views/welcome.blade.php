<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<!-- <link rel="icon" href="assets/img/favicon.ico"> -->
<title>Promikaris</title>
<!-- Bootstrap core CSS -->
<link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">
<!-- Fonts -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" rel="stylesheet">

<link href="https://fonts.googleapis.com/css?family=Righteous" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="{{ asset('/css/mediumish.css') }}" rel="stylesheet">
<!-- <link href="assets/css/style.css" rel="stylesheet"> -->
</head>
<body>

<!-- Begin Nav
================================================== -->
<nav class="navbar navbar-toggleable-sm navbar-light bg-white fixed-top mediumnavigation">
	<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
	<span class="navbar-toggler-icon"><i style="color: white" class="fas fa-bars"></i></span>
	</button>
	<div class="container">
		<!-- Begin Logo -->
		<a class="navbar-brand" href="/">
			<img src="{{ asset('/img/logopromikaris.png') }}" class="img-fluid" alt="logo">
		</a>
		<!-- End Logo -->
		<div class="collapse navbar-collapse" id="navbarsExampleDefault">
			<!-- Begin Menu -->
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a class="nav-link" href="#news">News</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#about">About</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#feature">Feature</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#contact">Contact</a>
				</li>
				@if ( isset($datauser))
				<li class="nav-item">
					<a class="nav-link" href="/home">Dashboard</a>
				</li>				
				@else
				<li class="nav-item">
					<a class="nav-link" href="/login">Login</a>
				</li>				
				@endif															
								
			</ul>
			<!-- End Menu -->
			<!-- Begin Search -->		
			<!-- End Search -->
		</div>
	</div>
</nav>
	<!-- End Nav
================================================== -->
<div class="jumbotron jumbotron-fluid py-0">
	<img src="{{ asset('/img/bg.jpg') }}" class="img-fluid" alt="">
	<!-- <div class="container"> -->
	  <!-- <h1 class="display-4 text-white">Promikaris</h1> -->
	  <!-- <p class="lead text-white">Listrik berada disekitar kita, mari jaga listrik seperti menjaga diri sendiri.</p> -->
	<!-- </div> -->
</div>

<!-- Begin Site Title
================================================== -->
<div class="container">
	<!-- <div class="mainheading">
		<h1 class="sitetitle">Promikaris</h1>
		<p class="lead">
			 Informate Computer Engineer to You
		</p>
	</div> -->
<!-- End Site Title
================================================== -->

	<!-- Begin Featured
	================================================== -->	<section class="featured-posts">
	<div class="section-title" id="news">
		<h2><span>News</span></h2>
	</div>	
	<div class="container">
		<div class="row">
			@foreach ($databerita as $berita)                				
			<div class="col-lg-4 listrecent pt-3">				
				<!-- begin post -->
				<div class="card">
						<img class="img-fluid" src="{{ asset('berita/'.$berita->path_gambar) }}" alt="">
					<div class="card-block">
						<h2 class="card-title"><a href="{{ $berita->link_berita }}">{{ substr($berita->judul, 0, 30).' ...' }}</a></h2>
						<h4 class="card-text">{{ implode(' ', array_slice(explode(' ', $berita->konten), 0, 15)) }}</h4>
						<div class="metafooter">
							<div class="wrapfooter">
								<span class="meta-footer-thumb">
								<a href="author.html"><img class="author-thumb" src="https://www.gravatar.com/avatar/e56154546cf4be74e393c62d1ae9f9d4?s=250&amp;d=mm&amp;r=x" alt="Sal"></a>
								</span>
								<span class="author-meta">
								<span class="post-name"><a href="author.html">Admin</a></span><br/>
								<span class="post-date">{{ $berita->created_at }}</span><span class="dot"></span>
								</span>							
							</div>
						</div>
					</div>
				</div>		
			<!-- end post -->			
			</div>
			@endforeach
		</div>
	</div>

                				
	
	</section>
	<!-- End Featured
	================================================== -->

	<!-- Begin List Posts
	================================================== -->

	<!-- End List Posts
	================================================== -->

	
	<div class="row about text-center" id="about">
		<div class="col-12">
			<h1 class="display-4 font-sec">About</h1>
		</div>
		<hr>
		<div class="col-12">
			<div class="mage-desc">
				<img class="mage-desc-img" src="{{ asset('/img/tanya.png') }}" alt="">
				<div class="mage-desc-container">
				  <div class="mage-desc-shadow"></div>
				  <div class="mage-desc-content">
					<p><span class="mage-desc-content-bold font-desc"></span> Program Mitigasi Cerdas Kebakaran Akibat Korsleting Listrik (PROMIKARIS)
						 merupakan sebuah Program Kreatifitas Mahasiswa guna mendukung masyarakat menjadi peduli bahaya arching serta mengetahui instalasi listrik yang baik dan benar. 
						Di dalam kegiatan juga diberikan sentuhan teknologi 4.0 sebagai sarana keberhasilan program.</p>
				  </div>
				</div>
			</div>
		</div>

	</div>

	<div class="row feature text-center" id="feature">
		<div class="col-12">
			<h1 class="display-4 font-sec">Feature</h1>
		</div>
		<hr>
		<div class="col-12">
			<div class="row">
				<div class="col-lg-4">
					<div class="row">
						<div class="col-12">
							<img src="/assets/img/Asset 71.png" alt="" class="feature-image">
						</div>
						<div class="col-12 d-flex justify-content-center">
							<div class="feature-button button" onclick="window.location.href='/home/lapor'">
								Report
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="row">
						<div class="col-12">
							<img src="/assets/img/Asset 72.png" alt="" class="feature-image">
						</div>
						<div class="col-12 d-flex justify-content-center">
							<div class="feature-button button" onclick="window.location.href='/home/konsul'">
								Konsul
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="row">
						<div class="col-12">
							<img src="/assets/img/Asset 75.png" alt="" class="feature-image">
						</div>
						<div class="col-12 d-flex justify-content-center">							
							<div class="feature-button button">
								<a style="color: white" href="http://intip.in/promikaris">Quiz</a>
							</div>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row contact text-center" id="contact">
		<div class="col-12">
			<h1 class="display-4 font-sec">Contact</h1>
		</div>
		<hr>
		<div class="col-12">
			<div class="d-flex flex-column align-items-center">
				<div class="d-flex justify-content-between w-50">
					<div class="h3 font-contact">
						<i class="fas fa-envelope mr-3"></i>Email : 
					</div>
					<div class="h3 font-contact">
						promikarisits@gmail.com
					</div>
				</div>
				<div class="d-flex justify-content-between w-50 mt-md-5 mt-sm-2">
					<div class="h3 font-contact">
						<i class="fab fa-instagram mr-3"></i>Instagram :
					</div>
					<div class="h3 font-contact">
						Promikaris ITS
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Begin Footer
	================================================== -->
	<div class="footer">
		<p class="pull-left">
			 Copyright &copy; 2020 Promikaris
		</p>		
		<div class="clearfix">
		</div>
	</div>
	<!-- End Footer
	================================================== -->

</div>
<!-- /.container -->

<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{ asset('/js/jquery.min.js') }}"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script> -->
<script src="{{ asset('/js/bootstrap.min.js') }}"></script>
<!-- <script src="assets/js/ie10-viewport-bug-workaround.js"></script> -->
</body>
</html>
