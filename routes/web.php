<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// auth route
Auth::routes();

Route::get('/', 'HomeController@landingpage');
Route::get('/berita/{judul}', 'HomeController@beritadetail');


//home user
Route::get('/home', 'HomeController@index');

//home admin
Route::get('/admin', 'AdminController@index')->name('admin')->middleware('auth', 'admin');


//Berita dashboard admin 
Route::get('/admin/berita', 'BeritaController@index')->middleware('auth', 'admin');
Route::get('/admin/berita/tambah', 'BeritaController@beritatambah')->middleware('auth', 'admin');
Route::post('/admin/berita/store', 'BeritaController@beritastore')->middleware('auth', 'admin');
Route::get('/admin/berita/edit/{id}', 'BeritaController@beritaedit')->middleware('auth', 'admin');
Route::put('/admin/berita/update/{id}', 'BeritaController@beritaupdate')->middleware('auth', 'admin');
Route::get('/admin/berita/hapus/{id}', 'BeritaController@beritadelete')->middleware('auth', 'admin');

//Konsul dashboard user
Route::get('/home/konsul', 'KonsulController@index')->middleware('auth','home');
Route::get('/home/konsul/tambah', 'KonsulController@konsultambah')->middleware('auth','home');
Route::post('/home/konsul/store', 'KonsulController@konsulstore')->middleware('auth','home');
Route::get('/home/konsul/edit/{id}', 'KonsulController@konsuledit')->middleware('auth','home');
Route::put('/home/konsul/update/{id}', 'KonsulController@konsulupdate')->middleware('auth','home');
Route::get('/home/konsul/hapus/{id}', 'KonsulController@konsuldelete')->middleware('auth','home');

//Konsul dashboard admin
Route::get('/admin/konsul', 'KonsulController@adminindex')->middleware('auth', 'admin');
Route::get('/admin/konsul/edit/{id}', 'KonsulController@adminkonsuledit')->middleware('auth', 'admin');
Route::put('/admin/konsul/update/{id}', 'KonsulController@adminkonsulupdate')->middleware('auth', 'admin');
Route::get('/admin/konsul/hapus/{id}', 'KonsulController@adminkonsuldelete')->middleware('auth', 'admin');

//lapor dashboard user
Route::get('/home/lapor', 'LaporController@index')->middleware('auth','home');
Route::get('/home/lapor/tambah', 'LaporController@laportambah')->middleware('auth','home');
Route::post('/home/lapor/store', 'LaporController@laporstore')->middleware('auth','home');
Route::get('/home/lapor/edit/{id}', 'LaporController@laporedit')->middleware('auth','home');
Route::put('/home/lapor/update/{id}', 'LaporController@laporupdate')->middleware('auth','home');
Route::get('/home/lapor/hapus/{id}', 'LaporController@lapordelete')->middleware('auth','home');


//lapor dashboard admin
Route::get('/admin/lapor', 'LaporController@adminindex')->middleware('auth', 'admin');
Route::get('/admin/lapor/edit/{id}', 'LaporController@adminlaporedit')->middleware('auth', 'admin');
Route::get('/admin/lapor/hapus/{id}', 'LaporController@adminlapordelete')->middleware('auth','admin');
Route::get('/admin/lapor/csv', 'LaporController@downloadCsv')->middleware('auth', 'admin');